"""Configuration for Shexia."""

import toolforge

USER_AGENT = toolforge.set_user_agent(
    'shexia',
    email='fn@imm.dtu.dk')
