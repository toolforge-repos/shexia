"""shex.

Usage:
  program shex <e>
  program shex-ids

"""

from functools import lru_cache

import requests

from config import USER_AGENT


def api_all_shex_ids():
    """Download a list of all ShEx identifiers.

    Download a list of all ShEx identifiers by querying the Wikidata API
    for a list of all pages in the Entity Schema namespace.

    Result
    ------
    ids : list of str
        List of Wikidata Entity Schema identifiers.

    Notes
    -----
    Currently only returns up to 500 identifiers.

    Examples
    --------
    >>> ids = api_all_shex_ids()
    >>> "E65" in ids
    True

    """
    url = "https://www.wikidata.org/w/api.php"
    params = {
        "action": "query",
        "list": "allpages",
        "apnamespace": "640",
        "aplimit": "500",
        "format": "json",
    }
    response = requests.get(
        url, params=params,
        headers={'User-Agent': USER_AGENT})
    if response.ok:
        response_data = response.json()
        result_data = response_data['query']['allpages']
        ids = [datum['title'][13:] for datum in result_data]
        return ids
    else:
        return []


@lru_cache(maxsize=None)
def download_shex_data(e):
    """Download ShEx data from Wikidata.

    Download JSON data from an entity schema on www.wikidata.org and return the
    data as a python dictionary.

    Parameters
    ----------
    e : str
        Identifier for the ShEx entity schema.

    Result
    ------
    shex_data : dict or None
        Dictionary with data from entity schemas on www.wikidata.org.

    Examples
    --------
    >>> shex_data = download_shex_data('E15')
    >>> shex_data['id'] = 'E15'
    True

    """
    url = "https://www.wikidata.org/w/index.php"
    params = {
        'title': 'EntitySchema:' + e,
        'action': 'raw'}
    response = requests.get(
        url, params=params,
        headers={'User-Agent': USER_AGENT})
    if response.ok:
        return response.json()
    else:
        return None


if __name__ == '__main__':
    from docopt import docopt

    arguments = docopt(__doc__)

    if arguments["shex-ids"]:
        ids = api_all_shex_ids()
        for id in ids:
            print(id)
    else:
        shex_data = download_shex_data(arguments['<e>'])
        print(shex_data['schemaText'])
